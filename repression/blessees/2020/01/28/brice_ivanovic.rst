.. index::
   pair: Brice Ivanovic ; Blessé(e)s

.. _brice_ivanovic_2020_01_28:

===========================================================================================
Mardi 28 janvier 2020 Brice Ivanovic choqué par l'explosion d'une GM2L à 1 mètre de lui
===========================================================================================

.. seealso::

   - https://twitter.com/bi1192/status/1222191784505245696?s=20
   - https://twitter.com/bi1192
   - :ref:`gm2l`

.. contents::
   :depth: 3

Mardi 28 janvier 2020
======================


Explosion d'une #GM2L à 1m de moi. Perte d'équilibre et confusion.
Fin de couverture pour moi.


.. figure:: brice_ivanovic/brice_ivanovic.png
   :align: center

   https://twitter.com/bi1192/status/1222191784505245696?s=20


Vendredi 31 janvier 2020  tweet https://twitter.com/bi1192/status/1223198313006161920?s=20
============================================================================================

Reveillé dans la nuit par douleur extrême oreille gauche, comme si mon tympan
explosait.


Vendredi 31 janvier 2020  tweet https://twitter.com/bi1192/status/1223198313006161920?s=20
==============================================================================================

::

    Allo @Place_Beauvau
     - c'est pour un signalement.

    Perte d'audition oreille gauche comprise entre 10 et 15db.
    Scanner lundi matin pour dommages oreille interne.

    Puissance onde de choc sonore : entre 160 et 170 db.

    Merci @davduf


.. figure:: brice_ivanovic/brice_ivanovic_2020_01_31.png
   :align: center

   https://twitter.com/bi1192/status/1223198313006161920?s=20

