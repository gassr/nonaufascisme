.. index::
   pair: Armes de guerre ; Grenade GLIF4

.. _glif4:

==========================================================================================================================================================
Grenade instantanée **GLI-F4** remplacée par la **GM2L** depuis le 26 janvier 2020 classée comme arme de catégorie A2 et donc comme *matériel de guerre*
==========================================================================================================================================================

.. seealso::

   - https://maintiendelordre.fr/grenade-instantanee-gli-f4-sae-810-alsetex/
   - https://twitter.com/Maxime_Reynie
   - https://paris-luttes.info/suite-au-15-aout-a-bure-autopsie-8576
   - :ref:`gm2l`


.. figure:: glif4.png
   :align: center


.. contents::
   :depth: 3

Description
=============

:Source: https://maintiendelordre.fr/grenade-instantanee-gli-f4-sae-810-alsetex/


La grenade **GLI-F4 SAE 810** est une grenade lacrymogène instantanée produite par
l’entreprise française Alsetex.

Elle équipe les forces de l’ordre depuis 2011 et n’est plus produite depuis
2014, elle reste cependant encore utilisée par le gouvernement jusqu’à
épuisement des stocks et est remplacée progressivement par la grenade :ref:`GM2L <gm2l>`
de Alsetex.

Avec ses 30 grammes d’explosifs dont 26 grammes de TNT et 10 g  de CS pur la
France était le seul pays européen à en faire l’usage en maintien de l’ordre.

Elle peut être utilisée à la main ou avec un lanceur Cougar 56mm.

La GLI-F4 est classée comme arme de catégorie A2 et donc comme “matériel de guerre“.

La GLI F4 est constituée d’une ogive en plastique rigide de couleur grise,
contenant une cartouche en polystyrène blanc dans laquelle on trouve le
détonateur et le dispositif d’allumage avec un couvercle de couleur jaune.

Généralement, les deux cartouches sont pulvérisées lors de l’explosion et on
ne trouve sur le sol que l’ogive en plastique et le propulseur qui a permis
de l’envoyer, ou le bouchon et la goupille si la grenade a été envoyée
à la main.


Si l’ordre de tir est donné par l’autorité civile, c’est-à-dire le préfet ou
son représentant, les policiers et gendarmes doivent avant tout procéder à
deux sommations.

Sur le terrain, le commandement peut prendre l’initiative de tir **sans sommation,**
si «des violences ou voies de fait sont exercées contre eux ou s’ils ne
peuvent défendre autrement le terrain qu’ils occupent».

- 339 Grenades utilisées par les CRS à Paris le 1er décembre 2018
- 3000 Grenades de ce type (GLI-F4 & GM2L) auraient été utilisé en avril 2018
  sur la ZAD de Notre-Dame-des-Landes
- 8 Mains arrachées par la GLI-F4


La GLI-F4 possède un triple effet : lacrymogène, sonore et détonnant.

Avec 165 décibels  “brisant et cassant” à 5 mètres elle surpasse le bruit d’un
avion au décollage et dépasse le seuil de douleur sonore.
Au delà de 120 dB, des bruits très brefs provoquent immédiatement des dommages
irréversibles. Lors de l’explosion elle libère 10 grammes de CS pulvérulent.

Lancée à la main le bouchon allumeur (0sec) actionne instantanément le corps
de la grenade qui ne ressemble alors qu’à un tube blanc en polystyrène à
l’embout jaune et qui mettra 2,5 secondes pour détonner.


D’après le constructeur, “aucun éclat n’est émis lors de la détonation”.
Cependant de nombreuses personnes blessées recensées par le journaliste
David Dufresne, le collectif Désarmons-les ! et de nombreux StreetMedics,
font état de blessures dues à des éclats en plastiques et métaux lors de
détonation de GLI-F4.

Le 11 juillet 2018, l’Institut de recherche criminelle de la gendarmerie
nationale stipule que l’onde de choc générée fragmente les tissus, les os
et provoque des blessures très importantes dont la létalité dépend de
la zone impactée.

Le 30 novembre 2018, un collectif d’avocats demande au président
Emmanuel Macron l’interdiction de cette grenade.
Le 17 mai 2019, une requête au conseil d’Etat pour demander la fin de
l’utilisation des GLI-F4 est rejetée.


Dans un rapport de janvier 2018 intitulé “substitution de la grenade GLI-F4 par
la grenade GM2L”, Alsetex informe le ministère de l’intérieur qu’il a arrêté de
produire la GLI-F4 remplacée par la :ref:`GM2L <gm2l>`.

Cette substitution est due à plusieurs choses.
Le décès de Nathalie Desiles en juin 2014 pendant la manipulation d’une
composition pyrotechnique sur le site d’Alsetex est l’élément déclencheur.
La grenade comporte aussi des défauts identifiés mais non résolus dont seule
la France est cliente.

Plusieurs accidents sont aussi mis en avant par le constructeur qui indique
donc qu’il a abandonné la GLI-F4 car “considérant le travail à faire pour
reprendre une production au regard du potentiel de ce produit, l’abandon s’impose…”.

“Pour ces raisons, Alsetex a decidé de substituer la grenade GLI-F4 56mm par la
grenade GM2L 56mm au fonctionnement et aux performances identiques sans les
principaux inconvénients de la GLI-F4 (exportabilité, utilisation, transport, coûts…).”

La grenade est définitivement retirée le 24 janvier 2020.

