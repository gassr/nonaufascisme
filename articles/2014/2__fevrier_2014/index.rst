
.. index::
  pair: Actions; Antifa (Février 2014)

.. _antifa_france_fevrier_2014:

==================================================
Actions/nouvelles Antifa France février 2014
==================================================


.. contents::
   :depth: 3

Affiche CLAF
============


.. figure:: riposte_sociale_antifasciste_CLAF.png
   :align: center
   

.. _comm_claf_fevrier_2014:

Communiqué de la Coordination Libertaire Antifasciste (Février 2014)
====================================================================

:download:`Communiqué de la CLAF (février 2014) <COMMUNIQUE_DE_LA_CLAF.pdf>`


AGRESSION à L'ARME BLANCHE DE DEUX MILITANTS ANTIFASCISTES à LYON
------------------------------------------------------------------

Vendredi 14 février, deux jeunes mineurs ont été poignardés dans le quartier 
Saint-Jean à Lyon par des militants d’extrême-droite, nécessitant leur 
hospitalisation et intervention chirurgicale. 

Un quartier que veulent s'approprier les fascistes et où se trouve notamment le
local «la Traboule» des identitaires.

Depuis plusieurs années, les attaques physiques imputables à l’extrême-droite 
ne cessent de se multiplier sur tout le territoire: assassinat de Clément Méric 
en juin dernier à Paris, attaque à l'arme à feu à Clermont-Ferrand le 17 janvier, 
attaques de bars et lieux associatifs, agressions islamophobes, antisémites et 
homophobes.

Tout semble permis à l'extrême-droite. 

Nous entrons dans une phase de lutte contre la renaissance des idées racistes, 
réactionnaires et fascisantes. 

La complaisance de la caste politique et judiciaire est lamentable.

En effet, en menant une politique pro-patronale (renégociation des accords 
Unedic, pacte de responsabilité...) et raciste (expulsions, chasse aux Rroms...), 
l’État donne aux fascistes une légitimation idéologique."

Devant les considérables dégâts sociaux causés par le capitalisme et la 
politique pro-patronale de l'Etat, seule une riposte de classe nous permettra 
de contrer le développement des idées fascistes.


LA COORDINATION LIBERTAIRE ANTIFASCISTE apporte son soutien à ces deux militants 
et appelle à la manifestation contre tous les fascismes.

Samedi 22 février à 14h00, Lyon place du Pont.

CLAF:

- Alternative libertaire
- Coordination des Groupes Anarchistes
- Confédération Nationale du Travail   
