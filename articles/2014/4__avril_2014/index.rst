
.. index::
   pair: Actions; Antifa (Avril 2014)

.. _antifa_france_avril_2014:

==================================================
Actions/nouvelles Antifa France avril 2014
==================================================


.. contents::
   :depth: 3

Derrière le fascisme
=====================

.. figure:: Tract_derriere_le_fascisme.jpg
   :align: center


Derrière le fascisme 2
========================

.. figure:: Tract_derriere_le_fascisme_2.jpg
   :align: center
