
.. index::
   pair: Article; Le néolibéralisme est un fascisme
   pair: There is no alternative ; TINA

.. _article_fascisme_2016_03_03:

===========================================================================================================================================
**Le néolibéralisme est un fascisme** par Manuela Cadelli, présidente de l’Association syndicale des magistrats (Belgique) le 3 mars 2016
===========================================================================================================================================

.. seealso::

   - https://www.lesoir.be/art/1137303/article/debats/cartes-blanches/2016-03-01/neoliberalisme-est-un-fascisme


.. contents::
   :depth: 3


Préambule
==========

Le temps des précautions oratoires est révolu ; il convient de nommer les choses
pour permettre la préparation d’une réaction démocrate concertée, notamment
au sein des services publics.

Le libéralisme était une doctrine déduite de la philosophie des Lumières, à la
fois politique et économique, qui visait à imposer à l’Etat la distance nécessaire
au respect des libertés et à l’avènement des émancipations démocratiques.

Il a été le moteur de l’avènement et des progrès des démocraties occidentales.


Le néolibéralisme est cet économisme total qui frappe chaque sphère de nos
sociétés et chaque instant de notre époque. **C’est un extrémisme**.

Le fascisme se définit comme l’assujettissement de toutes les composantes de
l’État à une **idéologie totalitaire et nihiliste**.

**Je prétends que le néolibéralisme est un fascisme** car l’économie a proprement
assujetti les gouvernements des pays démocratiques mais aussi chaque parcelle
de notre réflexion.
L’État est maintenant au service de l’économie et de la finance qui le traitent
en subordonné et lui commandent jusqu’à la mise en péril du bien commun.

L’austérité voulue par les milieux financiers est devenue une valeur supérieure
qui remplace la politique. Faire des économies évite la poursuite de tout autre
objectif public.
Le principe de l’orthodoxie budgétaire va jusqu’à prétendre s’inscrire dans
la Constitution des Etats. La notion de service public est ridiculisée.

Le nihilisme qui s’en déduit a permis de congédier l’universalisme et les valeurs
humanistes les plus évidentes : solidarité, fraternité, intégration et respect
de tous et des différences. Même la théorie économique classique n’y trouve
plus son compte : le travail était auparavant un élément de la demande, et les
travailleurs étaient respectés dans cette mesure ; la finance internationale
en a fait une simple variable d’ajustement.

Déformation du réel
=====================

Tout totalitarisme est d’abord un dévoiement du langage et comme dans le roman
de Georges Orwell, le néolibéralisme a sa novlangue et ses éléments de
communication qui permettent de déformer le réel.
Ainsi, toute coupe budgétaire relève-t-elle actuellement de la modernisation
des secteurs touchés.
Les plus démunis ne se voient plus rembourser certains soins de santé et
renoncent à consulter un dentiste ? C’est que la modernisation de la sécurité
sociale est en marche.

L’abstraction domine dans le discours public pour en évincer les implications
sur l’humain. Ainsi, s’agissant des migrants, est-il impérieux que leur
accueil ne crée pas un appel d’air que nos finances ne pourraient assumer.
De même, certaines personnes sont-elles qualifiées d’assistées parce qu’elles
relèvent de la solidarité nationale.

Culte de l’évaluation
=======================

Le darwinisme social domine et assigne à tous et à chacun les plus strictes
prescriptions de performance : faiblir c’est faillir.
Nos fondements culturels sont renversés : tout postulat humaniste est disqualifié
ou démonétisé car le néolibéralisme a le monopole de la rationalité et du
réalisme.
Margaret Thatcher l’a indiqué en 1985 : **There is no alternative (TINA)**. Tout le
reste n’est qu’utopie, déraison et régression.
Les vertus du débat et de la conflictualité sont discréditées puisque l’histoire
est régie par une nécessité.

Cette sous-culture recèle une menace existentielle qui lui est propre : l’absence
de performance condamne à la disparition et dans le même temps, chacun est
inculpé d’inefficacité et contraint de se justifier de tout.

La confiance est rompue.

L’évaluation règne en maître, et avec elle la bureaucratie qui impose la
définition et la recherche de pléthore d’objectifs et d’indicateurs auxquels
il convient de se conformer.
**La créativité et l’esprit critique sont étouffés par la gestion**.
Et chacun de battre sa coulpe sur les gaspillages et les inerties dont il
est coupable.

La Justice négligée
====================

L’idéologie néolibérale engendre une normativité qui concurrence les lois
du parlement. La puissance démocratique du droit est donc compromise.
Dans la concrétisation qu’ils représentent des libertés et des émancipations,
et l’empêchement des abus qu’ils imposent, le droit et la procédure sont
désormais des obstacles.

De même le pouvoir judiciaire susceptible de contrarier les dominants doit-il
être maté.
La justice belge est d’ailleurs sous-financée ; en 2015, elle était la dernière
d’un classement européen qui inclut tous les états situés entre l’Atlantique
et l’Oural. En deux ans, le gouvernement a réussi à lui ôter l’indépendance que
la Constitution lui avait conférée dans l’intérêt du citoyen afin qu’elle
joue ce rôle de contre-pouvoir qu’il attend d’elle.
Le projet est manifestement celui-là : qu’il n’y ait plus de justice en Belgique.

Une caste au-dessus du lot
============================

La classe dominante ne s’administre pourtant pas la même potion qu’elle prescrit
aux citoyens ordinaires car austérité bien ordonnée commence par les autres.
L’économiste Thomas Piketty l’a parfaitement décrit dans son étude des inégalités
et du capitalisme au XXIe siècle (Seuil 2013).

Malgré la crise de 2008, et les incantations éthiques qui ont suivi, rien ne
s’est passé pour policer les milieux financiers et les soumettre aux exigences
du bien commun. Qui a payé ? Les gens ordinaires, vous et moi.

Et pendant que l’État belge consentait sur dix ans des cadeaux fiscaux de 7
milliards aux multinationales, le justiciable a vu l’accès à la justice surtaxé
(augmentation des droits de greffe, taxation à 21 % des honoraires d’avocat).
Désormais pour obtenir réparation, les victimes d’injustice doivent être riches.

Ceci dans un Etat où le nombre de mandataires publics défie tous les standards
mondiaux. Dans ce secteur particulier, pas d’évaluation ni d’études de coût
rapportée aux bénéfices.
Un exemple : plus de trente ans après le fédéralisme, l’institution provinciale
survit sans que personne ne puisse dire à quoi elle sert.
La rationalisation et l’idéologie gestionnaire se sont fort opportunément
arrêtées aux portes du monde politique.


Idéal sécuritaire
===================

Le terrorisme, cet autre nihilisme qui révèle nos faiblesses et notre couardise
dans l’affirmation de nos valeurs, est susceptible d’aggraver le processus en
permettant bientôt de justifier toutes les atteintes aux libertés, à la contestation,
de se passer des juges qualifiés inefficaces, et de diminuer encore la protection
sociale des plus démunis, sacrifiée à cet « idéal » de sécurité.

Le salut dans l’engagement
===========================

Ce contexte menace sans aucun doute les fondements de nos démocraties mais
pour autant condamne-t-il au désespoir et au découragement ?

Certainement pas.

Voici 500 ans, au plus fort des défaites qui ont fait tomber la plupart des
Etats italiens en leur imposant une occupation étrangère de plus de trois siècles,
Nicolas Machiavel exhortait les hommes vertueux à tenir tête au destin et,
face à l’adversité des temps, à préférer l’action et l’audace à la prudence.

Car plus la situation est tragique, plus elle commande l’action et le refus
de *s’abandonner* (Le prince, chapitres XXV et XXVI).

Cet enseignement s’impose à l’évidence à notre époque où tout semble compromis.
La détermination des citoyens attachés à la radicalité des valeurs démocratiques
constitue une ressource inestimable qui n’a pas encore révélé, à tout le moins
en Belgique, son potentiel d’entraînement et sa puissance de modifier ce qui
est présenté comme inéluctable.
Grâce aux réseaux sociaux et à la prise de parole, chacun peut désormais
s’engager, particulièrement au sein des services publics, dans les universités,
avec le monde étudiant, dans la magistrature et au barreau, pour ramener le
bien commun et la justice sociale au cœur du débat public et au sein de
l’administration de l’État et des collectivités.

**Le néolibéralisme est un fascisme**. Il doit être combattu et un humanisme total
doit être rétabli.

