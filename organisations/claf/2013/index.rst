
.. index::
  pair: CLAF; 2013

.. _claf_2013:

=========================================================
Coordination Libertaire Antifasciste (2013)
=========================================================

.. contents::
   :depth: 3



Courriel
========

::

    Sujet:  [Liste-syndicats] Adhésion de la CNT à la Coordination Libertaire Antifasciste & appel à candidature
    Date :  Fri, 13 Dec 2013 14:57:19 +0100
    De :    Secrétaire Confédéral CNT <cnt@cnt-f.org>

Aux syndicats & Unions de la CNT,

CherEs camarades,

Vous trouverez ci-dessous et en pièce jointe le mail adressé ce jour à la 
Coordination Libertaire Antifasciste (CLAF) pour engager notre adhésion 
et notre participation.

La décision a été prise ce week-end à l'occasion du CCN de Lille.  
Nous vous en communiquerons le compte-rendu dans les meilleurs délais.

Déjà deux syndicats se sont portés candidats pour participer des travaux de 
la CLAF sous couvert du BC.

Nous lançons de même un appel à l'ensemble des syndicats, si des camarades 
souhaitaient (sur présentation de leur syndicat) s'engager dans les travaux 
de la CLAF aux côté du BC. Le cas échéant, merci de nous contacter.

Un tirage de matériel intégrant les signatures CNT, CGA, AL est donc prévu.
 
Nous vous tiendrons informés prochainement des modalités de diffusion dans 
les Régions.

Vous souhaitant bonne réception,
Salutations AS & SR


