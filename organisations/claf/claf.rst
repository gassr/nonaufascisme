
.. index::
   pair: Coordination ; Antifascisme
   ! CLAF
   ! Coordination Libertaire Antifasciste

.. _claf:

=========================================================
Coordination Libertaire Anti-Fasciste (CLAF)
=========================================================

.. seealso::

   - :ref:`claf`

.. toctree::
   :maxdepth: 3

   2017/2017
   2014/index
   2013/index




