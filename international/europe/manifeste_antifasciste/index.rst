
.. index::
   pair: Manifeste; Européen 
   pair: Antifa; Europe

.. _manifeste_antifasciste_europeen_2013:

============================================
Manifeste antifasciste européen (mars 2013)
============================================

.. seealso::

   - http://www.visa-isa.org/node/14385
   - :ref:`visa`


Soixante huit ans après la fin de la deuxième guerre mondiale et la 
défaite du fascisme et du nazisme on assiste presque partout en Europe 
à la montée de l’extrême droite. 

Mais, phénomène encore plus inquiétant, on voit se développer à la droite 
de cette extrême droite des forces carrément néo-nazies qui, dans certains 
cas (Grèce, Hongrie,...) s’enracinent dans la société formant des vraies 
mouvements populaires de masse, radicaux, racistes, ultra-violents et 
pogromistes dont l’objectif déclaré est la destruction de toute organisation 
syndicale, politique et culturelle des travailleurs, l’écrasement de 
toute résistance citoyenne, la négation du droit à la différence et 
l’extermination, même physique, des **différents et des plus faibles**.

Comme dans les années ’20 et ’30, la cause génératrice de cette menace 
néo-fasciste et d’extrême droite est la profonde crise économique, sociale, 
politique et aussi éthique et écologique du capitalisme lequel, prenant 
prétexte de la crise de la dette, est en train de mener une offensive 
sans précédent contre le niveau de vie, les libertés et les droits des 
travailleurs, contre tous ceux d’en bas !  

Profitant de la peur des nantis face aux risques d’explosion sociale, 
ainsi que de la radicalisation des classes moyennes laminées par la 
crise et les politiques d’austérité draconienne, et du désespoir des 
chômeurs marginalisés et paupérisés, l’extrême droite et les forces 
néo-nazies et néo-fascistes se développent dans toute l’Europe ; 

ils acquièrent une influence de masse dans les couches déshéritées 
qu’elles tournent systématiquement contre des boucs émissaires traditionnels 
et nouveaux (les immigrés, les musulmans, les Juifs, les homosexuels, 
les handicapés,...) ainsi que contre les mouvements sociaux, les organisations 
de gauche et les syndicats ouvriers.

L’influence et la radicalité de cette extrême droite ne sont pas les mêmes 
partout en Europe. Cependant, la généralisation des politiques d’austérité 
draconienne a comme conséquence que la montée de l’extrême droite soit 
déjà un phénomène presque général. 

La conclusion est évidente : Le fait que la montée impétueuse de l’extrême 
droite et l’émergence d’un néofascisme ultra-violent de masse ne soit plus 
l’exception à la règle européenne, oblige les antifascistes de ce continent 
à affronter ce problème à sa juste dimension, c'est-à-dire en tant que 
problème européen !

Mais, dire ça ne suffit pas, il faut ajouter que la lutte contre l’extrême 
droite et le néonazisme est d’une urgence absolue. 
En effet, dans plusieurs pays européens la menace néofasciste est déjà 
si directe et immédiate qu’elle transforme la lutte antifasciste en combat 
de toute première priorité, dont l’enjeu est la vie ou la mort de la gauche, 
des organisations ouvrières, des libertés et des droits démocratiques, 
des valeurs de solidarité et de tolérance, du droit à la différence. 

Dire qu’on est engagé dans une course de vitesse contre la barbarie raciste 
et néofasciste correspond désormais à une réalité vérifiée chaque jour 
dans les rues de nos villes européennes…

Vue la profondeur de la crise, les dimensions des dégâts sociaux qu’elle 
provoque, l’intensité de la polarisation politique, la détermination et 
l’agressivité des classes dirigeantes, l’importance des enjeux historiques 
de l’affrontement en cours et l’ampleur de la montée des forces d’extrême 
droite il est évident que le combat antifasciste constitue un choix 
stratégique exigeant un sérieux organisationnel et un investissement 
politique et militant à long terme. 

En conséquence, la lutte antifasciste doit être étroitement liée au combat 
quotidien contre les politiques d’austérité et le système qui les génère.

Pour être efficace et répondre aux attentes de la population, la lutte 
antifasciste doit être organisée de manière unitaire et démocratique et 
être le fait des masses populaires elles-mêmes. 

Pour ce faire, les citoyens et les citoyennes doivent organiser leur lutte 
antifasciste et leur auto-défense eux-mêmes. 

En même temps, pour être efficace cette lutte doit être globale, s’opposant 
à l’extrême droite et au néofascisme sur tous les terrains où se manifeste 
le poison du racisme et de la de l’homophobie, du chauvinisme et du militarisme, 
du culte de la violence aveugle et de l’apologie des chambres à gaz et 
d’Auschwitz. 

En somme, pour être efficace à long terme, le combat antifasciste doit 
proposer une autre vision de la société, diamétralement opposée à celle 
proposée par l’extrême droite : C'est-à-dire, une société fondée sur la 
solidarité, la tolérance et la fraternité, le refus du machisme, le rejet 
de l’oppression des femmes et le respect du droit à la différence, 
l’internationalisme et la protection scrupuleuse de la nature, la défense 
des valeurs humanistes et démocratiques.

Ce mouvement antifasciste européen doit être l’héritier des grandes 
traditions antifascistes de ce continent ! 

Pour ce faire, il devrait poser les bases d’un mouvement social doté des 
structures, ayant une activité quotidienne, pénétrant toute la société, 
organisant les citoyens antifascistes en réseaux selon leurs professions, 
leurs habitations et leurs sensibilités, menant combat sur tous les fronts 
des activités humaines et assumant pleinement la tache de la protection 
même physique des plus vulnérables de nos concitoyens, des immigrés, 
des Roma, des minorités nationales, des musulmans, des Juifs ou des 
homosexuels, de tous ceux et celles qui sont systématiquement victimes 
du racisme d’Etat et de la pègre fasciste.

C’est donc parce que le besoin de la mobilisation antifasciste à l’échelle 
européenne se fait chaque jour plus pressant que nous, qui signons ce 
manifeste, nous appelons à la constitution d’un Mouvement Antifasciste 
Européen unitaire, démocratique et de masse, capable d’affronter et de 
vaincre la peste brune qui relève la tète sur notre continent. 

Nous ferons tout pour que le congrès constitutif de ce Mouvement Antifasciste 
Européen dont on a tant besoin, se tienne à Athènes au printemps 2013 et 
soit couplé d’une grande manifestation antifasciste européenne dans les 
rues de la capitale grecque.


