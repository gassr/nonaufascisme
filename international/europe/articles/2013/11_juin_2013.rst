

=========================================================
En France comme ailleurs en Europe, l’extrême droite tue
=========================================================

Chers amis,

Voici la tribune que j'ai écrite suite au meurtre de Clément Méric par 
des skinheads à Paris mercredi dernier, pour tenter de donner un éclairage 
européen à la violence politique qui s'est alors déployée, et pour laquelle 
toute l'extrême droite française, dont bien évidemment le Front National, 
porte une lourde responsabilité.

Vous la trouverez à la suite de cet email, en fichier joint et en suivant 
le lien sur nouvelobs.com où elle est publiée : 

http://leplus.nouvelobs.com/contribution/884324-mort-de-clement-meric-en-france-et-en-europe-ne-laissons-plus-l-extreme-droite-tuer.html.

La tribune a été publiée ou le sera très prochainement dans de nombreux 
pays européens : Grande-Bretagne, Portugal, Belgique, Grèce, Albanie, 
Suisse, Turquie,...

N'hésitez pas à revenir vers moi à votre convenance, comme à l'accoutumée, 
pour tout commentaire ou toute question.

Amitiés,

Benjamin Abtan
Président du Mouvement Antiraciste Européen EGAM (European Grassroots Antiracist Movement)
@ : benjamin_abtan@yahoo.fr
Cell : +33 7 60 83 20 80
Skype : benjamin.abtan

 
En France comme ailleurs en Europe, l’extrême droite tue
=========================================================
 
par Benjamin Abtan, Président du Mouvement Antiraciste Européen EGAM
 
 
Stupeur, mais aucunement surprise : l’extrême droite a tué en France comme 
elle tue ailleurs en Europe. Le jeune Clément est mort sous les coups de 
skinheads en plein Paris. Qui peut feindre l’étonnement ? Qui peut dire 
que ce passage à l’acte d’une violence extrême n’était pas prévisible, 
prévu même ? Qui peut être surpris de la résurgence des crimes politiques 
d’extrême droite en France ?
 
Le meurtre de Clément s’inscrit dans la montée de la violence brune au 
niveau européen. L’extrême droite française dans son ensemble, des 
groupuscules skinheads à la « manif pour tous » en passant par le 
Front National, a légitimé, accompagné, suscité cette violence et porte 
ainsi une lourde responsabilité dans ce meurtre. 

Aujourd’hui comme hier, cette famille politique est le camp du crime.
 
Comment être surpris de cet abominable assassinat ?
 
Nous savons bien qu’Alexandre Gabriac, dirigeant des Jeunesses Nationalistes, 
s’est rendu en Grèce en décembre dernier pour s’inspirer des néo-Nazis 
d’Aube Dorée. 
Antisémitisme structurant, symbole inspiré de la svastika, salut nazi, 
racisme virulent, négationnisme, nostalgie du IIIe Reich, ils combinent 
l’action légale et illégale, la présentation aux élections et les 
violentes et quotidiennes agressions contre les immigrés, dans les rues 
d’Athènes notamment.

Régulièrement, ils tuent. Comment s’étonner du meurtre de Clément ? Ils 
tuent à Athènes, ils tuent à Paris.
 
Dès leur entrée au parlement en mai dernier, nous n’avons cessé de faire 
vivre une solidarité européenne avec les démocrates grecs, mais les 
néo-Nazis continuent de progresser, notamment au sein de la jeunesse. 

En France, après une exclusion de façade, Alexandre Gabriac continue 
d’être fortement lié au groupe FN au Conseil régional de Rhône-Alpes.
 
 
Nous savons également bien que, en Hongrie, la Garde hongroise, milice 
paramilitaire du Jobbik, qui revendique sa filiation d’avec le Parti 
des croix fléchées qui organisa l’extermination des juifs et des roms 
pendant la seconde guerre mondiale, terrorise les Roms, les oblige 
souvent à fuir le pays, et en assassine régulièrement.

Ils tuent dans les villages en Hongrie, ils tuent à Paris. 
Comment en être surpris ?

L’extrême droite française est alliée au Jobbik, qui est la pierre 
angulaire de la stratégie de développement européen du FN avec qui il a 
notamment fondé, à Budapest en 2009, « l’Alliance européenne des mouvements nationaux ».
 
 
Nous avons tous en mémoire le massacre d’Oslo et des jeunes sociaux-démocrates 
à Utoya, en Norvège, à l’été 2011. Ici déjà, la jeunesse était la cible, 
et le crime, politique. 
Reprenant les délires racistes sur la guerre civile européenne que les 
blancs chrétiens devraient mener contre les immigrés et les musulmans, 
le tueur a été célébré comme un héros par la fachosphère.
Le Président d’honneur du FN avait alors fait porter la responsabilité 
du massacre non pas sur le tueur, mais sur l’immigration, et Marine Le Pen 
l’avait soutenu.
Il a tué à Utoya, le FN soutient. Ils tuent à Paris. Qui peut croire à 
l’innocence du FN en ce qui concerne ces crimes politiques ?
Nous nous sommes rendus auprès des rescapés, peu après le massacre.
En Europe, nous avons été bien seuls à le faire.
 
 
En France, la mobilisation contre l’ouverture du mariage civil aux couples 
homosexuels a été l’occasion de l’expression de toutes les haines : 
des homosexuels, des femmes, de la démocratie, des immigrés, de la laïcité,…
C’est cette mobilisation qui a permis à la violence qui se déploie en 
Europe de trouver la légitimité et la base sociale pour se déployer en France.
 
L’écho donné aux propos haineux jusque dans l’enceinte de l’Assemblée Nationale, 
les défilés côté à côte de dirigeants de partis républicains et de 
partis antirépublicains comme le FN, la trop faible condamnation des 
violences de fin de manifestations qui étaient des violences politiques 
visant la République, l’acceptation de la présence de tous les 
antirépublicains, de tous les racistes, de tous les nervis d’extrême 
droite ont préparé, rendu possible, autorisé le meurtre de Clément.
 
Avec le choc, doit venir le renouvellement de l’engagement.
 
Celui des autorités publiques tout d’abord. Un peu plus d’un an après 
l’affaire Merah, l’Europe tourne de nouveau son regard vers la France 
et attend une réaction digne de son plus beau visage.

Il est fondamental que les autorités publiques reprennent leur soutien, 
tant en France qu’au niveau européen, à la lutte contre le racisme et 
pour la démocratie. Il est crucial d’enfin donner corps à deux thématiques 
qui ont animé la campagne de François Hollande : la jeunesse, touchée 
aujourd’hui au cœur, et l’égalité, violentée par des mois de funeste 
mobilisation réactionnaire.
Enfin, il est évidemment de salubrité publique d’interdire les groupes 
d’extrême droite qui portent la responsabilité du meurtre de Clément.
 
C’est également le temps du renouvellement de l’engagement de la société 
civile, trop passive dans son opposition à la montée de l’extrême droite 
en France comme ailleurs sur notre continent. 

Il ne faut laisser passer aucun appel à la haine, aucun acte délictueux, 
car aucun n’est innocent. Il ne faut pas se laisser impressionner par 
le sentiment de puissance et de permissivité qui anime les fossoyeurs 
de la démocratie car nous, démocrates, sommes les plus nombreux, en 
France comme dans le reste de l’Europe.
 
L’enjeu de ce nécessaire engagement renouvelé est clair : la liberté, 
la démocratie et la vie. Faisons en sorte que Clément soit le denier à 
tomber sous les coups de l’extrême droite.
