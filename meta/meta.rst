.. index::
   ! meta-infos


.. _antifa_meta_infos:

=====================
Meta infos
=====================

.. contents::
   :depth: 3

.. seealso::

   - https://cnt-f.gitlab.io/meta/


Gitlab project
================

.. seealso::

   - https://framagit.org/france1/nonaufascisme


Issues
--------

.. seealso::

   - https://framagit.org/france1/nonaufascisme/-/boards


Sphinx theme : **sphinx_book_theme**
============================================

.. seealso::

   - https://gdevops.gitlab.io/tuto_documentation/doc_generators/sphinx/themes/sphinx_book_theme/sphinx_book_theme.html


::

    liste_full = [
        "globaltoc.html",
    html_theme = "sphinx_book_theme"
    copyright = f"2011-{now.year}, assr38, Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme=}"



root directory
===============

::

    $ ls -als

::


    $ ls -als
    total 184
    4 drwxr-xr-x 16 pvergain pvergain  4096 nov.  30 10:37 .
    4 drwxr-xr-x  5 pvergain pvergain  4096 nov.  30 10:27 ..
    4 drwxr-xr-x  8 pvergain pvergain  4096 nov.  30 10:26 articles
    4 drwxr-xr-x  3 pvergain pvergain  4096 nov.  30 10:38 communiques
    8 -rwxr-xr-x  1 pvergain pvergain  4281 nov.  30 10:37 conf.py
    76 -rw-r--r--  1 pvergain pvergain 76971 nov.  30 10:26 contre_les_tyrans.png
    4 -rw-r--r--  1 pvergain pvergain    93 oct.  27 18:29 feed.xml
    4 drwxr-xr-x  8 pvergain pvergain  4096 nov.  30 10:42 .git
    4 -rwxr-xr-x  1 pvergain pvergain    49 nov.  30 10:26 .gitignore
    4 -rw-rw-rw-  1 pvergain pvergain   211 oct.  26 19:58 .gitlab-ci.yml
    4 drwxr-xr-x  3 pvergain pvergain  4096 nov.  30 10:39 groupes
    4 drwxr-xr-x  2 pvergain pvergain  4096 oct.  19 18:09 index
    4 -rwxr-xr-x  1 pvergain pvergain   887 nov.  30 10:26 index.rst
    4 drwxr-xr-x  3 pvergain pvergain  4096 nov.  30 10:39 international
    4 -rw-rw-rw-  1 pvergain pvergain  1154 mars  30  2020 Makefile
    4 drwxr-xr-x  2 pvergain pvergain  4096 nov.  30 10:41 meta
    4 drwxr-xr-x  3 pvergain pvergain  4096 nov.  30 10:39 militantEs
    4 drwxr-xr-x  4 pvergain pvergain  4096 nov.  30 10:39 organisations
    4 -rw-rw-rw-  1 pvergain pvergain  1017 oct.  26 20:03 .pre-commit-config.yaml
    4 -rw-rw-rw-  1 pvergain pvergain   307 oct.  26 20:01 pyproject.toml
    4 -rw-r--r--  1 pvergain pvergain    32 nov.  30 10:26 README.md
    4 drwxr-xr-x  4 pvergain pvergain  4096 nov.  30 10:39 regions
    4 drwxr-xr-x  4 pvergain pvergain  4096 nov.  30 10:26 repression
    4 -rw-r--r--  1 pvergain pvergain   558 nov.  30 10:26 requirements.txt
    4 drwxr-xr-x  4 pvergain pvergain  4096 nov.  30 10:40 rlf
    4 drwxr-xr-x  4 pvergain pvergain  4096 nov.  30 10:40 sites
    4 drwxr-xr-x  4 pvergain pvergain  4096 nov.  30 10:26 tracts


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
