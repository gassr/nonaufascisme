
.. index::
   pair: Antifa; LUCIDE
   ! LUCIDE
    
.. _lucide_pub:   
   
=====================================================
LUtte Contre les IDées d'Extrême droite (LUCIDE, 38) 
===================================================== 

.. seealso::

   - http://www.raslfront-isere.org/blog/lire-article-557584-9806628-lucide___collectif_iserois_de_lutte_contre_les_ide.html
  

.. figure:: lucide.png
   :align: center

:contact: lucide.grenoble@hotmail.fr   

.. toctree::
   :maxdepth: 3
   
   plateforme/index
   2014/index
   2013/index
   
   
