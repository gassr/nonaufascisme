.. index::
   pair: Antifa; 1er juin 2013


.. _tract_lucide_1er_juin_2013:

==================================================
Marine Le pen n'est pas la bienvenue à Grenoble !
==================================================

.. contents::
   :depth: 3

Texte
=====

:download:`Tract du 1er juin 2013 <tract_LUCIDE_premier_juin_2013.pdf>`


Le Pen 2ème génération et le Front National s’invitent à Grenoble dans 
sa "tournée des oubliés" pour un rassemblement privé le samedi 1er juin 2013.

Nous considérons qu'ils n'ont pas leur place dans une ville Compagnon de 
la Libération. Ni ici, ni ailleurs !

Le Front National est considéré comme un parti comme un autre en République 
française (élu-e-s dans les instances, financement public, ...). 

N'oublions pas que le parti national-socialiste est arrivé démocratiquement
au pouvoir en Allemagne par les **élections de 1933**.

Dans un contexte de crise globale, le Front National surfe sur les politiques 
d’austérité et anti-immigrations pour faire passer ses idées xénophobes, 
réactionnaires et sexistes. 

Avec sa "tournée des oubliés", il joue sur la misère généralisée qui en 
résulte pour rendre responsable de la dégradation de nos conditions de 
vie:

- les immigré-e-s, 
- les syndicalistes, 
- les femmes, 
- les LGBT (Lesbiennes Gay, Bi et Transsexuelles), 
- les  différent-e-s de la société, 
- ...

**Il cherche à diviser le "peuple d’en bas" pour nous détourner de nos vrais 
adversaires**.

Les "solutions" du FN sont contraires aux intérêts du monde du travail 
et de nos concitoyen-ne-s.

Derrière un discours soi-disant social, **son programme réel n’est qu’une 
défense du libéralisme le plus sauvage**. Son projet politique est de 
construire un État autoritaire réduisant à néant les libertés démocratiques
et nos droits sociaux.

Que toutes les forces démocratiques, politiques, syndicales, associatives, 
que toutes celles et ceux conscients des enjeux se rejoignent ici et maintenant 
et opposent un front de lutte sans faille, pour juguler les forces
mortifères des droites extrêmes, lepéniste et autres, avant qu'elles ne 
gagnent encore du terrain. 

S’opposer pied à pied est nécessaire mais un combat efficace nécessite 
de lutter contre les politiques d'austérité et de casse sociale menées 
depuis plusieurs décennies.

De même que, solidairement, nous ne cesserons de dénoncer et combattre 
les méthodes de l'extrême droite : intimidations, violences verbales, 
psychologiques et physiques envers celles et ceux qui ne pensent pas 
comme eux ou s'opposent à eux, envers celles et ceux qui veulent que la 
République, grâce au vecteur de la démocratie, prenne tout son sens premier.

**On sait comment ça commence, on sait comment ça finit**.

**Le temps est venu où chaque citoyen-ne ne peut plus dire "je ne savais pas", 
ne peut plus rester en dehors de la lutte déterminée contre l'extrême 
droite et ses idées**.

LUCIDE, collectif de LUtte Contre les Idées D’Extrême droite, sait qu’il 
peut compter sur toutes vos forces pour participer à un RASSEMBLEMENT 
samedi 1er Juin 2013 16H30 PLACE VICTOR HUGO GRENOBLE

suivi d’un défilé et clos par UN MOMENT FESTIF au JARDIN DE VILLE

Premiers signataires 
=====================

- ADDIRP, 
- CGT,
- CIIP, 
- GU, 
- JC, 
- Mouvement de la Paix, 
- NPA, 
- Osez le Féminisme, 
- PAG, 
- PCF, 
- PG, 
- Ras l'front Isere
- Solidaires Isère
- UNL
