
.. index::
   pair: LUCIDE; Clément Méric


.. _tract_lucide_clement_meric:

=======================
L’extrême droite tue !
=======================

.. seealso::

   - :ref:`clement_meric`

Ce Mercredi 5 juin 2013 en fin d’après-midi, un de nos jeunes camarades 
syndicalistes, :ref:`Clément Méric <clement_meric>`, a été assassiné en 
plein Paris par des  militants d’extrême-droite. 

Âgé de 19 ans et étudiant en première année de sciences politiques, il a 
été battu à mort en sortant d'une boutique à proximité de la gare St Lazare, 
à Paris, par cinq militants ouvertement néo-fascistes.

Nous entendons déjà les réactions « indignées » émanant de part et 
d’autre du cirque médiatico-politique faisant mine de s’étonner que de 
tels actes puissent survenir en France aujourd’hui. 

Ce n’est que pure hypocrisie de la part de journaux et de certains 
politiques qui participent activement à l'islamophobie, à la discrimination 
des minorités, à la stigmatisation des quartiers populaires et qui couvrent 
d’un honteux  silence les rafles dont sont victimes les roms et les 
sans-papiers. 

Ils contribuent activement à la banalisation des thèses d’extrême-droite, 
désignant indirectement des boucs-émissaires comme responsables de la crise.

Quant au gouvernement, comment explique-t-il que des groupes  militarisés 
qui prônent et pratiquent ouvertement la violence et la provocation depuis 
maintenant plus de vingt ans aient pignon sur rue ? 
Il y a une complaisance policière évidente envers ces groupes qui multiplient 
les coups de forces dans tout le pays. 

Nous demandons que le gouvernement prenne toute ses responsabilités.

Nous ne nous laisserons pas intimider par les fascistes et nous continuerons 
partout à combattre leurs thèses mensongères, réactionnaires. 

Derrière une posture « anti-système », l’extrême droite appliquera les 
directives des capitalistes, des banques, des marchés financiers, et ce 
de manière plus autoritaire.
Nous continuerons à dénoncer les vrais responsables de la crise, de l'austérité.

Nous exigeons : 

- l'arrêt des poursuites des militants antifascistes, notamment les 
  camarades de Lyon et de Grenoble
- l'instruction des plaintes concernant les mouvements d’extrême droite
- la dissolution des groupes violents d'extrême droite
  
  
Cet assassinat montre la nécessité de s'organiser pour combattre les 
groupes et les idées d’extrême droite. C'est par une mobilisation 
quotidienne contre les idées de haine que nous vaincrons.

Rassemblement Vendredi 7 juin à partir de 18h30 place Félix Poulat à Grenoble

Premiers signataires 
---------------------

- Solidaires Étudiant, 
- US Solidaires Isère, 
- JC, 
- La patate chaude, 
- PRCF, 
- CIIP, 
- Comité de soutien aux sans papiers, 
- comité de soutien aux algériens, 
- Alternative libertaire, 
- Ras le Fascisme, 
- PG, 
- NPA, 
- GU, 
- SOS Racisme, 
- PCF
