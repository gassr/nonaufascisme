 
.. index::
   pair: Antifascisme; Rhône (69, Février 2014)
   
   
.. _antifa_rhone_fevrier_2014:

==================================================
L’ANTIFASCISME C’EST  L’AFFAIRE DE TOUTES ET TOUS
==================================================


Après l'agression au couteau à St Jean, appel à manifestation contre le 
fascisme et ses violences

Depuis plusieurs années, nous assistons à une augmentation des agressions 
graves commises par des militants d’extrême droite dans toute la France. 

Assassinat de Clément Méric à Paris, attaque à l’arme à feu à Clermont Ferrand 
contre un concert de soutien aux sans-papiers, attaques de bars et lieux 
associatifs (le « vice et versa » à Lille, le « Buck Mulligan’s » à Tours…) 
militants et simples citoyens pris pour cible pour leurs idées ou leur 
apparence physique.

Ces agressions s’inscrivent dans un contexte de mobilisations réactionnaires 
(Manif pour tous, Jour de colère…) décomplexant les discours d’extrême droite.

L’absence de réaction concrète des pouvoirs en place ont favorisé un sentiment 
de légitimité et d’impunité à l’origine de l’escalade de cette violence.

Lyon n’échappe pas à ce constat : agression de 3 syndicalistes à St Jean en 2010, 
tentative de meurtre à Villeurbanne, passage à tabac en bande, avec des barres 
de fer, d’un ancien militant, agression physique d’une journaliste, agression 
raciste d’un couple rue Duguesclin.

Vendredi 14 février 2014, deux jeunes ont été poignardés par des militants 
d’extrême droite à St Jean. Cette agression est la dernière en date d’une longue 
série dans ce quartier. Des groupes d’extrême droite tentent depuis plusieurs 
années de semer la terreur dans celui-ci, qu’ils cherchent à s’approprier. 
L’exemple le plus frappant étant l’existence depuis 3 ans du local identitaire 
« La Traboule » Montée du Change.

Ces événements sont intolérables et nous dénonçons la zone de non-droit qu’est 
devenu le quartier Saint Jean. La mairie et la préfecture de Jean restent sans 
réaction malgré le fait que nombre de collectifs et d’associations les aient 
alertés  depuis plusieurs années.

nous dénoncons la zone de non-droit qu'Lyon restent sans réaction malgré le fait 
que nombre de collectifs et d’associations les aient alertés  depuis plusieurs années.
Nous dénonçons l’absence de réaction concrète des pouvoirs publics.
Nous dénonçons la complaisance de certaines cours de justice envers les auteurs 
de ces violences.
Nous dénonçons les propos et les politiques racistes de l’État qui entretiennent 
ce climat nauséabond.

L’espace public n’appartient pas aux fascistes
Reprenons la rue, reprenons nos quartiers
MANIFESTATION  SAMEDI 22 FEVRIER
14h PLACE DU PONT (Guillotière)

