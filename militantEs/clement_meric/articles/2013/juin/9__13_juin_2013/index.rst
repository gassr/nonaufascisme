
.. index::
   pair: Clément Méric; 13 juin 2013
   pair: Antifa; 13 juin 2013

.. _clement_m_13_juin_2013:

=================================================
Clément M, la naissance d’un militant libertaire
=================================================

.. seealso::

   - http://www.fondation-besnard.org/article.php3?id_article=1889


Clément M, la naissance d’un militant libertaire

Quand il entre à 15 ans au lycée à Brest, il y a de cela quatre ans,
Clément joue de la basse avec son groupe de musique et fréquente des
militants d’Alternative Libertaire.

Ce qu’il entend au niveau national, ce sont les discours fascisants sur
l’ "Identité Nationale" de l’époque Sarkozy. Ce qu’il voit, c’est la lutte
pour la retraite à 60 ans.

Pendant ses années de lycée, Clément milite en soutien aux sans-papiers.

Il devient aussi un végétalien convaincu.

Il est dans les luttes écologiques et de défense des animaux.

En classe de première, ses premiers pas en politique sont stoppés net
par la maladie. Son 16ème anniversaire, il le passe en chambre stérile à
lutter contre une leucémie.
Au début on croît le perdre. Le choc est rude et le moral à zéro.
Mais il se bat et il se bat aussi pour que sa guitare puisse entrer dans
sa chambre. On stérilise donc la guitare ! Des mois de traitements lourds,
des mois de chambre stérile et le soutien des élèves de sa classe, des
profs et des amis. Après des mois de lutte et de courage, le voilà de
retour au lycée.
Il retrouve son cocktail préféré à Brest : musique et politique. Et il
fonce pour croquer la vie.

Bac en poche, il veut entrer à Science Po à Paris. Lors de l’entretien
d’admission il affirme ses doutes sur notre société ainsi que son opposition
au système économique capitaliste.
Il parle des conditions de vie des sans-papiers.
Il se présente sans équivoque comme libertaire. Dans un entretien d’admission,
c’est risqué mais il est admis. Faut croire que Sciences Po a besoin de
se donner l’image d’une officine de formation ouverte et tolérante.

A Sciences Po, qu’il surnomme très vite "PIPO", au milieu de beaucoup de
"nés bourgeois" comme il l’écrit, Clem cherche des ami-e-s et un syndicat
étudiant.

"L’unef beurk ! La CNT, à Pipo y a pas ! Un Sud-étudiant va se remonter.

La première réunion est dans quelques heures, j’y serai". Il fréquente
aussi la CNT. C’est là qu’il va commencer à discuter avec des militants
antifascistes. Très vite, Clem entre dans l’AFA "action anti fasciste".

Son engagement c’est sa force et il fonce !

Par ailleurs Clément ne peut pas rester longtemps sans jouer de la basse.
Il trouve et retrouve des amis musiciens et participe à de nombreux
concerts "off".

Au début 2013 les mails de Clément portent essentiellement sur les manifs
contre le mariage pour tous et sur la lutte contre l’homophobie :
citons "Qu’ils nous laissent libres d’aimer qui on veut. D’ailleurs ce
n’est pas à l’Etat de s’occuper de ça".

Clément découvre avec horreur le nazisme affiché des fascistes, les
croix gammées gravées dans la peau, les saluts nazis. Il est traumatisé
par les "ratonnades" auxquelles ces fascistes se livrent contre des
jeunes originaires du nord de l’Afrique.
Il ajoute "L’usage que le FN et les "groupustules" fachos font du drapeau
français, c’est leur "cache secte nazie".

Dans un dernier mail, il écrit : "je m’ennuie dans les cours de macro-éco :
c’est pipeau. En plus je commence à détester le discours dominant de
Pipo." A se demander alors s’il arrivera à supporter cela trois, quatre
ans.

Clément préfère l’action aux palabres. Cependant l’étudiant calme, doux,
discret, qui paraît si fragile, a des propos incisifs, raides parfois.
Mais des propos qui résultent d’une analyse politique fine sur la France
et l’Europe. Voici, tiré de ses derniers mails ...

Sur la vie syndicale à Brest il écrit sur sa page Face Book :"L’arsenal
n’est plus aussi rouge qu’avant, mais c’est un devoir de mémoire."

Sur la lutte antifasciste : "Cette lutte n’a pas l’air d’intéresser les
partis et syndicats de la gauche classique. Des discours oui, mais pas
d’actes. Que sont-ils devenus ?"

Sur sa lutte AntiFa : "Etre AntiFa ce n’est pas une posture morale,
c’est une lutte politique et de tous les jours. Pour tous les jeunes
d’Europe ! T’as vu en Hongrie, en Angleterre, tous ces fachos ?"

Sur la construction de la gauche : "Depuis la chute du mur de Berlin,
la gauche est dans la broyeuse : débris et divisions partout.
A la CNT, au NPA, au front de gauche, c’est partout la merde. Nous ne
construirons pas une autre politique à gauche avec les gravats du mur !"

Sur les sans-papier et le mariage pour tous : "On se bat pour l’égalité
des droits, mais le mariage je m’en cale. Les manifs des droites sont
des démonstrations de force homophobes.

Finalement on dit mariage pour tous, papiers pour tous, mais ce que nous
préférons, c’est pas de papiers du tout, pour personne et pas de mariage
du tout !"

Dans ce parcours politique de quatre ans,

quatre petites années,

un militant politique libertaire est né .......

et ........ ils lui ont volé sa vie le 5 juin 2013.

Il avait 18 ans.

(8 juin 2013. JB. Extraits de courriels entre Clément et un ami de la
famille)

