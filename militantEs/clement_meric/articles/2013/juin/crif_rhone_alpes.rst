


==============================================
Clément Méric (Le CRIF Rhône-Alpes juin 2013)
==============================================

.. seealso::

   - http://www.crif.org/fr/lecrifenaction/le-crif-rh%C3%B4ne-alpes-interpelle-sur-les-d%C3%A9rives-antid%C3%A9mocratiques-qui-m%C3%A8nent-%C3%A0-la-violence/37451
   - http://www.crif.org/fr/tribune/cl%C3%A9ment-m%C3%A9ric-un-citoyen-du-monde-sachant-dire-non/37454



Le CRIF Rhône-Alpes s’associe à tous les républicains de ce pays et, en 
particulier à ceux de notre ville, pour condamner l’odieuse altercation 
dont la violence extrémiste a conduit au décès du jeune Clément Méric.


Le CRIF Rhône-Alpes trouve par ailleurs inacceptables les violences qui 
s’en sont suivies à l’encontre d’élus républicains et en particulier 
celles qui ont visé, jeudi soir, le Sénateur Maire de Lyon Gérard Collomb et son épouse.

 

Nous déplorons qu’il faille attendre la survenue d’évènements aussi 
dramatiques pour qu’enfin il y ait une prise de conscience générale de 
l’ambiance délétère qui envahit subrepticement notre société, ambiance 
au sujet de laquelle nous n’avons pas manqué, à maintes reprises, d’alerter.

 

Le CRIF Rhône-Alpes interpelle régulièrement les pouvoirs publics sur la 
multiplication dans la région Lyonnaise de manifestations et de réunions 
au cours desquelles se libère une parole de haine et de violence, source 
de toutes les dérives antidémocratiques.

 

Les raccourcis historiques, la désinformation volontaire et quotidienne 
sur tous les sujets sensibles, les amalgames de mots et de concepts des 
faiseurs d’opinions de tous bords ont fait le lit d’une violence que 
tous aujourd’hui ont bien du mal à contrôler.

 

Nous formulons tous nos vœux pour que l’apaisement reprenne très vite le dessus dans notre démocratie.

   
   

