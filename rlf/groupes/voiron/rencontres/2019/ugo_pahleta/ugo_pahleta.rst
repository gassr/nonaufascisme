
.. index::
   pair: Ugo Pahleta; 2019


.. _ugo_2019:

=============================================
Ugo Pahleta
=============================================


.. seealso::

   - https://www.infolibertaire.net/la-possibilite-du-fascisme-avec-ugo-palheta/


Mouvement réactionnaire de masse contre l’égalité des droits ; migrants enlevés,
tabassés et laissés pour mort par des milices à Calais ; large diffusion de
thèses réactionnaires, xénophobes et islamophobes ; intensification du
quadrillage répressif des quartiers populaires et violences policières impunies ;
manifestations interdites et criminalisation croissante de toute contestation ;
scores inégalés du Front national à toutes les élections depuis 2012.

Sous des formes disparates et encore embryonnaires, mais dont la seule
énumération dit le pourrissement actuel de la politique, c’est le fascisme
qui fait retour.

Et celui-ci s’annonce non comme une hypothèse abstraite mais comme une
possibilité concrète.

Pourtant, la possibilité du fascisme est généralement balayée d’un revers de
main par les commentateurs : comment la République française, patrie
autoproclamée des droits de l’homme, pourrait-elle engendrer le monstre
fasciste ?

La France ne s’est-elle pas montrée « allergique » au fascisme tout au long
du XXe siècle, comme le prétendent certains historiens français ?

Le Front national n’a-t-il pas renoncé au projet ultranationaliste, raciste et
autoritaire qui le caractérisait depuis sa création ?
N’assiste-t-on pas au renouveau du capitalisme français sous les auspices d’un
jeune président réalisant enfin les « réformes » prétendument nécessaires ?

C’est à démonter ces fausses évidences que s’attache ce livre, scrutant ainsi
la trajectoire d’un désastre possible, enraciné dans la triple offensive
néolibérale, autoritaire et raciste dont Emmanuel Macron est la parfaite
incarnation, mais un désastre résistible, pour peu que le danger soit reconnu
à temps et qu’émerge un nouvel antifascisme, capable de mener de front le
combat contre l’extrême droite et celui contre les politiques destructrices
qui favorisent son ascension.

*VISA 13* : VISA 13 est une déclinaison départementale du réseau VISA
(Vigilance et initiatives syndicales antifascistes)

VISA est une association intersyndicale composée d’une cinquantaine de
structures syndicales : la FSU et plusieurs de ses syndicats, l’Union Syndicale
Solidaires et plusieurs de ses syndicats, des fédérations et des syndicats de
la CGT, de la CFDT, de la CNT SO, de la CNT, de l’UNEF et le syndicat de
la Magistrature.


