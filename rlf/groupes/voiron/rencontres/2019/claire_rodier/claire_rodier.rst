
.. index::
   pair: Claire Rodier; 2019


.. _claire_rodier_2019:

=============================================
11e rencontres de Voiron le 12 janvier 2019
=============================================


.. seealso::

   - https://www.franceculture.fr/oeuvre/migrants-refugies-reponse-aux-indecis-aux-inquiets-et-aux-reticents


Ugo Palheta
------------

.. seealso::

   - https://www.infolibertaire.net/la-possibilite-du-fascisme-avec-ugo-palheta/


Mouvement réactionnaire de masse contre l’égalité des droits ; migrants enlevés,
tabassés et laissés pour mort par des milices à Calais ; large diffusion de
thèses réactionnaires, xénophobes et islamophobes ; intensification du quadrillage répressif des quartiers populaires et violences policières impunies ; manifestations interdites et criminalisation croissante de toute contestation ; scores inégalés du Front national à toutes les élections depuis 2012. Sous des formes disparates et encore embryonnaires, mais dont la seule énumération dit le pourrissement actuel de la politique, c’est le fascisme qui fait retour.

Et celui-ci s’annonce non comme une hypothèse abstraite mais comme une possibilité concrète. Pourtant, la possibilité du fascisme est généralement balayée d’un revers de main par les commentateurs : comment la République française, patrie autoproclamée des droits de l’homme, pourrait-elle engendrer le monstre fasciste ? La France ne s’est-elle pas montrée « allergique » au fascisme tout au long du XXe siècle, comme le prétendent certains historiens français ? Le Front national n’a-t-il pas renoncé au projet ultranationaliste, raciste et autoritaire qui le caractérisait depuis sa création ? N’assiste-t-on pas au renouveau du capitalisme français sous les auspices d’un jeune président réalisant enfin les « réformes » prétendument nécessaires ?

